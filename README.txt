********************************************************************
D R U P A L    M O D U L E
********************************************************************
Name: OG Legal Module 
Author: Derek Laventure
Sponsor: Registered Nurses' Association of Ontario [www.rnao.org]
Drupal: 6.x
********************************************************************
DESCRIPTION:

This module is based off of the excellent Legal module by Robert Castelo, and
much of the code is reused from that project.

A module which displays your Terms & Conditions to users who want to
access an Organic Group, and makes sure they accept the T&C before their
can see group content.

Note: No T&C will be displayed until the T&C text has been input by the
administrator. Managers of individual groups must also opt-in to enable T&Cs
for their group before the Terms & Conditions tab becomes available.
    
Each time a new version of the T&C is created all users will be required to 
accept the new version.

Note: T&C text should only be entered by administrators or other highly trusted users.
filter_xss_admin() is used to filter content for display, this is a very permissive 
XSS/HTML filter intended for admin-only use.



********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.  If you run into
problems, you should always read the INSTALL.txt that comes with the
Drupal package and read the online documentation.

	1. Place the entire og_legal directory into your Drupal
        modules/directory.

	2. Enable the legal module by navigating to:

	   Administer > Site building > Modules

	Click the 'Save configuration' button at the bottom to commit your
    changes.
    


********************************************************************
CONFIGURATION

	1. Go to Administer > User management > Access control
	    
	    Set which roles can "view group Terms and Conditions" (those you want to accept the T&C)
            Set which roles can "manage own group Terms and Conditions" (those who can create/update T&C for their groups)
	    Set which roles can "administer group Terms and Conditions" (those who can set sitewide defaults and manage all T&Cs)
	
	2. Go to Administer > Site configuration > OG Legal

	    Here you can set the default display style for all group T&Cs sitewide.

	3. Go to your Group node and click Edit, then check the 'Enable Terms
           and Conditions' checkbox. Click the Terms and Conditions tab which appears.

	   Input your terms & conditions text, set how you would like it
     displayed:

	- Scroll Box - Standard form text box (read only) Text is entered
    and displayed as text only

	- Scroll Box (CSS) - Scrollable text box created in CSS Text should
       be entered with HTML formatting. 
       (less accessible than a standard scroll box)

	- HTML Text - Terms & conditions displayed as HTML formatted text
        Text should be entered with HTML formatting

	-  Page Link - T&C page link on the Accept checkbox label, 
         T&Cs not displayed on registration page

	Note: When displayed on the page /legal your T&Cs will be automatically 
              reformatted to HTML Text if entered as a Scroll Box or Scroll Box (CSS)
       
       
       
********************************************************************
ACKNOWLEDGEMENTS

* Original Legal project, by Robert Castelo
http://drupal.org/project/legal

* Drupal 5 update sponsorship
Lullabot (http://www.lullabot.com)

* User data variables clean up
Steven Wittens (Steven)

* T&C Page formatting
Bryant Mairs (Susurrus) 




