<?php


/**
 * @file
 * Administration page callbacks for the og_legal module
 **/

/**
 * Form builder. Configure sitewide defaults for og_legal
 *
 * @ingroup forms
 * @see system_settings_form().
 **/
function og_legal_admin_settings() {
  $form['og_legal_display'] = array(
    '#type' => 'radios',
    '#title' => t('Default Display Style for Terms and Conditions'),
    '#default_value' => variable_get('og_legal_display', 0),
    '#options' => array(t('Scroll Box'), t('Scroll Box (CSS)'), t('HTML Text'), t('Page Link')),
    '#description' => t('How Terms & Conditions are displayed to users by default'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

